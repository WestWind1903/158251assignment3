import re 
import os 
import sys

def get_file_name(dir , file_extension):
    result_list = []
    for file_name in os.listdir(dir):
        if os.path.splitext(file_name)[1] == file_extension:
            result_list.append(os.path.join(dir, file_name))
    return result_list


def srt2vtt(file_name):
    content = open(file_name , "r", encoding="utf-8").read()
    
    # add WEBVTT\n\n 
    content = "WEBVTT\n\n" + content

    # replace "," with "."
    content = re.sub("(\d{2}:\d{2}:\d{2}),(\d{3})", lambda m: m.group(1) + '.' + m.group(2), content)

    output_file = os.path.splitext(file_name)[0] + '.vtt'
    open(output_file, "w", encoding="utf-8").write(content)




file_list = get_file_name("/media/song/Data/编程文件/158.258/158258Assign_3/WebContent/Videos", ".srt")
print(file_list)
for file in file_list:
    print(file)
    srt2vtt(file)
print("Done")



