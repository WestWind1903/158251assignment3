
window.onload = function autoWidth(){
    var winWidth=0;
    if (window.innerWidth)
        winWidth = window.innerWidth;
    else if ((document.body) && (document.body.clientWidth))
        winWidth = document.body.clientWidth;
    if (document.documentElement && document.documentElement.clientWidth)
        winWidth = document.documentElement.clientWidth;
    document.body.style.width = winWidth + "px";
};