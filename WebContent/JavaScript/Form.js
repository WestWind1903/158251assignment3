function storeRadio(name, prefix, time) {
    localStorage.setItem(time+"_"+prefix + "_" + name, $("[name=" + "'" + name + "'" + "]" + ":checked").val());

}

function getRadioValue(name) {
     return $("[name=" + "'" + name + "'" + "]" + ":checked").val();

}

function storeText(name, prefix, time) {
    const element = document.getElementsByName(name);
    localStorage.setItem(time+"_"+prefix + "_" + name, element[0].value);
}

function getTextValue(name) {
    const element = document.getElementsByName(name);
    return  element[0].value;
}

function storeTextArea(name, prefix, time) {
    localStorage.setItem(time+"_"+prefix + "_" + name, $("[name=" + "'" + name + "'" + "]").val())
}

function getTextAreaValue(name) {
    return $("[name=" + "'" + name + "'" + "]").val();
}



function PharmacySubmitJson() {
    var date = new Date();
    var time = date.toLocaleString();

    var form ={
        submitTime: time,
        fname:getTextValue("fname"),
        way:getRadioValue("way"),
        lname:getTextValue("lname"),
        address:getTextValue("address"),
        date:getTextValue("date"),
        massey:getRadioValue("massey"),
        message:getTextAreaValue("message"),
        phone:getTextValue("phone"),
        pills:getTextAreaValue("pills")
    };

    localStorage.setItem(time+"_"+"Pharmacy", JSON.stringify(form));
    alert("Pharmacy Requirement Submit successfully!")
}


function ReserveSubmitJson() {
    var date = new Date();
    var time = date.toLocaleString();

    var form ={
        submitTime: time,
        fname:getTextValue("fname"),
        gender:getRadioValue("gender"),
        lname:getTextValue("lname"),
        address:getTextValue("address"),
        date:getTextValue("date"),
        massey:getRadioValue("massey"),
        message:getTextAreaValue("case"),
        phone:getTextValue("phone"),
        detail:getTextAreaValue("detail")
    };

    localStorage.setItem(time+"_"+"Reserve", JSON.stringify(form));
    alert("Reserve Requirement Submit successfully!")
}


function VetSubmitJson() {
    var date = new Date();
    var time = date.toLocaleString();
    var form ={
        submitTime: time,
        fname:getTextValue("fname"),
        age:getTextValue("age"),
        lname:getTextValue("lname"),
        kind:getTextValue("kind"),
        date:getTextValue("date"),
        massey:getRadioValue("massey"),
        email:getTextValue("email"),
        case:getTextAreaValue("case"),
        detail:getTextAreaValue("detail")
    };

    localStorage.setItem(time+"_"+"Vet", JSON.stringify(form));
    alert("Vet Requirement Submit successfully!")
}



function ConsultationSubmitJson() {
    var date = new Date();
    var time = date.toLocaleString();
    var form ={
        submitTime: time,
        fname:getTextValue("fname"),
        gender:getRadioValue("gender"),
        lname:getTextValue("lname"),
        weight:getTextValue("weight"),
        height:getTextValue("height"),
        massey:getRadioValue("massey"),
        birthday:getTextValue("birthday"),
        case:getTextAreaValue("case"),
        detail:getTextAreaValue("detail")
    };

    localStorage.setItem(time+"_"+"Con", JSON.stringify(form));
    alert("Consultation Requirement Submit successfully!");
}
