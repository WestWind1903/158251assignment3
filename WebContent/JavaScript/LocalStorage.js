
var contextList = [];

function CreatePara(parentNode, context) {
    const para = document.createElement("p");
    para.id = "info";
    para.innerText = context;
    parentNode.appendChild(para);
    parentNode.appendChild(document.createElement("hr"));
}

function CreateParaJson(parentNode, context, tag) {
    const para = document.createElement(tag);
    para.id = "info";
    para.innerText = context;
    parentNode.appendChild(para);
}

function ShowEachForm(obj, parentNode) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            CreateParaJson(parentNode, key + ":  " + obj[key], "p");
    }
}

function ShowAllJson() {
    const parentNode = document.getElementById("show");
    var context;
    var obj;
    for (var i = 0; i < localStorage.length; i++) {
        context = JSON.parse(localStorage.getItem(localStorage.key(i)));
        obj = localStorage.getItem(localStorage.key(i));
        if (!(contextList.indexOf(obj) > -1)) {
            contextList.push(obj);
            CreateParaJson(parentNode, localStorage.key(i), "h2");
            ShowEachForm(context, parentNode);
            parentNode.appendChild(document.createElement("hr"));
            parentNode.appendChild(document.createElement("br"));
        }
    }
    if (contextList.length === 0) {
        alert("local Storage is empty!");
    }
}


function ClearAll() {
    const parentNode = document.getElementById("show");
    parentNode.innerHTML = "";
    localStorage.clear();
    contextList.splice(0, contextList.length);
    alert("Done! Click 'Show All' to Check")
}

